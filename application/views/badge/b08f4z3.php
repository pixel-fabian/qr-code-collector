<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_8_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Hütte</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Um Platz für die Hütte zu schaffen vergrößerte ich die Terrasse, indem ich den Hang abtrug.
      Dann legte ich 12 Punktfundamente für den Grundstock der Hütte und in 2 Tagen wurde 1998 mit kräftigem Einsatz der ganzen Familie, meinen Eltern und Geschwistern mit Kindern, die Hütte aufgebaut.
    </p>
    <p>
      Zuvor hatte ich ein Zisterne mit 1000 Liter im Boden versenkt. Das Wasser kann mit der Schwengelpumpe heraufgepumt werden.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_01.jpg" alt="Guetle">
      <figcaption>Fundament (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_02.jpg" alt="Guetle">
      <figcaption>Material für die Hütte (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_03.jpg" alt="Guetle">
      <figcaption>Günther & Franz bauen die Wände (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_04.jpg" alt="Guetle">
      <figcaption>Franz & Günther (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_05.jpg" alt="Guetle">
      <figcaption>Franz, Martin & Heinz decken das Dach (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_06.jpg" alt="Guetle">
      <figcaption>Martin, Lisa, Fabian & Ursula (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b08_07.jpg" alt="Guetle">
      <figcaption>Raphael, Yannick, Fabian, Ursula, Lisa & Gertrud (1998)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
