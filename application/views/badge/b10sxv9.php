<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_10_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Sonnensitzplatz</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Den früheren „Sandelplatz“ von Fabian haben wir dieses Jahr als zweiten Sitzplatz umgestaltet.
      Von hier schweift der Blick zum Container-Hafen, nach Obertürkheim zur Katholischen Kirche St. Franziskus und zur Evangelischen Petruskirche.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b10_01.jpg" alt="Guetle">
      <figcaption>Sandkasten (1998)</figcaption>
    </figure>
    <p>
      Auf dem Pflaumenbaum oberhalb der Hütte befand sich einige Jahre lang ein Baumhaus.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b10_02.jpg" alt="Guetle">
      <figcaption>Baumhaus & Sandkasten (1999)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b10_03.jpg" alt="Guetle">
      <figcaption>Fabian am Baumhaus (1996)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
