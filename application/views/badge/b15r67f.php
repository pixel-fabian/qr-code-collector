<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_15_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Aroniabeeren und Feigenbaum</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Der Aronia-Strauch stammt ursprünglich aus Nordamerika, kam vor rund 150 Jahren nach Russland und über Ostdeutschland zu uns. Die Beere enthält die Vitamine A, B2, K und C und hat hat einen hohen Anteil an Flavonoiden. Das sind sogenannte sekundäre Pflanzenstoffe, die eine antioxidative Wirkung haben.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b15_01.jpg" alt="Guetle">
      <figcaption>Aronia Beeren (2018)</figcaption>
    </figure>
    <p>
      Den Feigenbaum haben wir als Topfpflanze aus Italien mitgebracht. Er erfreute uns auf der Terrasse zu Hause  über Jahre mit seinen Früchten. Er wuchs und wuchs und wir pflanzten ihn 2012 hier in den Garten. Er gedeiht hier an seinem Standort gut und beschenkt uns, außer letztem Jahr durch den Frost, mit reichlich Früchten.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b15_02.jpg" alt="Guetle">
      <figcaption>Feigen (2018)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
