<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_2_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Unser Gütle</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Im Laufe der Jahre haben wir das Gütle zum Freizeitdomizil umgestaltet.
      Es ist ein Ort zum Gestalten, Ausprobieren, kreativ zu sein, zu pflanzen, zu graben.
      Es war ein Ort für Fabian und seine Freunde zum Spielen und Entdecken.
      Für Gertrud und mich ein Rückzugsort, um die „Seele baumeln“ zu lassen.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b02_01.jpg" alt="Guetle">
      <figcaption>Gertrud, Günther, Irmgard & Fabian (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b02_02.jpg" alt="Guetle">
      <figcaption>Fabian & Gertrud (1998)</figcaption>
    </figure>
    <p>
      Mäuse, Salamander und Blindschleichen leben in den Steinmauern, graben Tunnels und bringen damit die Steinmauern zum Einsturz. Der Aufbau der Trockenmauern begleiten mich über die Jahre. Mich inspiriert es immer wieder, neue Aspekte hinzuzufügen, wie hier die Tonne für Süßkartoffeln.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b02_03.jpg" alt="Guetle">
      <figcaption>Stützmauer (2002)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b02_04.jpg" alt="Guetle">
      <figcaption>Arbeit an den Mauern (2002)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
