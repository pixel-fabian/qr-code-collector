<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_16_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Stuttgart-Wangen</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Aufgrund der Hanglage ersetzt das Arbeiten im Gütle jedes Fitnesstudio. Hier oben wird man dafür mit einem Ausblick in das Neckartal mit dem <a href="https://de.wikipedia.org/wiki/Wangen_(Stuttgart)" target="_blank">Stadtteil Wangen</a> und dem Industriegebiet (Daimler) belohnt.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b16_02.jpg" alt="Guetle">
      <figcaption>Blick über den Garten (2015)</figcaption>
    </figure>
    <p>
      Von hier ist die Straßenbahn (gelb) zu sehen, die nach Hedelfingen und über Stuttgart-Ost nach Stuttgart fährt.
      Parallel verläuft die Ulmer-/Hedelfinger Straße, über die wir hier zum Fest gekommen sind.
      Dahinter liegt die Bundesstraße B 10 Stuttgart nach Ulm, zweispurig, sehr befahren, die Geräuschkulisse ist wahrnehmbar.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b16_01.jpg" alt="Guetle">
      <figcaption>Stuttgart Wangen (<a href="https://www.openstreetmap.org/#map=15/48.7712/9.2410" target="_blank">OpenStreetMap</a>)</figcaption>
    </figure>
    <p>
      Zwischen Containern, Schrottberg und Öl-/Benzintanks ist der <a href="http://www.neckar-kaeptn.de/start/ueber-uns/neckar/" target="_blank">Neckar</a> mit <a href="https://www.hafenstuttgart.de/" target="_blank">Hafen</a> zu erkennen.
      Ganz am Ende von der Talsohle führt die stark befahrene Zugstrecke Stuttgart – Ulm. Auf den Gleisen verkehrt auch die S-Bahn und der Güterverkehr.
    </p>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
