<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_7_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Wächter</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Die Sandsteinfigur stand ursprünglich auf der „Blauen-Säule“. Als der obere Pflaumenbaum morsch wurde und auf die Rebenpergola mit Spannseilen herunter krachte,
      stürzte er herab. Nun hat er seinen Platz unter dem unteren Pflaumenbaum gefunden und wacht über das Geschehen auf dem Gütle.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b07_01.jpg" alt="Guetle">
      <figcaption>Wächter (2010)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
