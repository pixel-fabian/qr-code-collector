<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_5_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">7 Frauen</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Die Auseinandersetzung mit dem Nussbaumstamm bestand darin, möglichst viele Personen dort unterzubringen.
      Entstanden sind sieben Frauen, die alle miteinander in Kontakt stehen und eine „tänzerische“ Darbietung vollführen.
      Ich habe sie geflammt, was eine Anspielung an das sich im Solarium Bräunen – Verbrennen ist. Sie stehen unter dem Lebensbaum (Thuja) als Schutz vor dem Regen und Schnee.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b05_01.jpg" alt="Guetle">
      <figcaption>7 Frauen, Nussbaum</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
