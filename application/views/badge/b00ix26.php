<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_0_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Los gehts!</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>Herzlich willkommen bei der Gütle Erkundung!</br>
      Zieh deine stabilen Schuhe an und schnapp dir ein Smartphone mit einer QR-Code-Scanner App, dann kann es direkt los gehen.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b00_01.jpg" alt="Guetle">
      <figcaption>Startpunkt am Waldheim Wangen</figcaption>
    </figure>
    <p>Folge dem Kopfsteinpflasterweg nach unten und halte nach weiteren QR-Codes Ausschau.</p>
    <p>Viel Spaß wünschen dir Heinz, Fabian & Gertrud.</p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b00_02.jpg" alt="Guetle">
      <figcaption>Hier geht es weiter</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
