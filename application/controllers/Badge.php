<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Badge extends CI_Controller {

	/**
	 * Konstruktur für Badges
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('badge_model');
	}

	/**
	* Index Page for this controller.
	*/
	public function index() {
		//Durch alle Badges loopen
		$badge_list = $this->badge_model->get_all_badges();
		$data['badges'] = array();

		//Alle Badges aktiv (mit Link) anzeigen
		for ($i = 0; $i < count($badge_list); $i++) {
			$data['badges'][] = array(
				'collected' => TRUE,
				'id' => $badge_list[$i],
				'img' => 'badge_'.$i.'_aktiv.jpg',
			);
		}
		// Select our view file
		$data['content'] = 'index_view';
		// Display the page with the above defined content
		$this->load->view('template_rahmen', $data);
	}

	/**
	 * Index Page for this controller.
	 * Badges im Cookie mit Link anzeigen
	 * Alle anderes Badges ohne Link
	 */
	public function index_old() {
		//Prüfen ob Cookie existiert
		if(is_null(get_cookie('CI_Badges'))) {
			$cookie_badges = '';
		} else {
			$cookie_badges = get_cookie('CI_Badges');
		}
		//Durch alle Badges loopen
		$badge_list = $this->badge_model->get_all_badges();
		$data['badges'] = array();

		for ($i = 0; $i < count($badge_list); $i++) {
			//Prüfen ob ID schon im Cookie vorhanden ist
			if(strpos($cookie_badges, $badge_list[$i]) === false){
				$collected_tmp = FALSE;
				$img_tmp = 'badge_'.$i.'_inaktiv.jpg';
			} else {
				$collected_tmp = TRUE;
				$img_tmp = 'badge_'.$i.'_aktiv.jpg';
			}
			$data['badges'][] = array(
				'collected' => $collected_tmp,
				'id' => $badge_list[$i],
				'img' => $img_tmp,
			);
		}
		// Select our view file
		$data['content'] = 'index_view';
		// Display the page with the above defined content
		$this->load->view('template_rahmen', $data);
	}

	/**
	 * Einzelansicht eines Badges
	 */
	public function details($id) {
		//Prüfen ob Badge mit dieser ID exisitert
		if($this->badge_model->badge_exists($id)) {
			//View des passenden Badges wählen
			$data['content'] = 'badge/'.$id;
		} else {
			//Fehlerseite ausgeben
			$data['content'] = 'error_view';
		}
		//Template-Rahmen mit View laden
		$this->load->view('template_rahmen', $data);
	}

	/**
	 * Einzelansicht eines Badges
	 * mit Cookie-Logik
	 */
	public function details_old($id) {
		//Prüfen ob Badge mit dieser ID exisitert
		if($this->badge_model->badge_exists($id)) {
			//Badge zu Cookie hinzufügen
			$this->add_badge($id);
			//View des passenden Badges wählen
			$data['content'] = 'badge/'.$id;
		} else {
			//Fehlerseite ausgeben
			$data['content'] = 'error_view';
		}
		//Template-Rahmen mit View laden
		$this->load->view('template_rahmen', $data);
	}

	/**
	 * Badge zu Cookie hinzufügen
	 */
	protected function add_badge($id) {
		//Prüfen ob Cookie existiert
		if(is_null(get_cookie('CI_Badges'))) {
			//Erste ID hinzufügen
			$cookie_badges = $id;
		} else {
			//Cookie abfragen
			$cookie_badges = get_cookie('CI_Badges');
			//Prüfen ob ID schon im Cookie vorhanden ist
			if(strpos($cookie_badges, $id) === false){
				//Neue ID an vorhandere IDs anhängen
				$cookie_badges = $cookie_badges.', '.$id;
			}
		}
		//Cookie setzen
		$cookie_expire = 30 * 24 * 60 * 60; //Gültigkeit 30 Tage
		$cookie = array(
			'name' => 'CI_Badges',
			'value' => $cookie_badges,
			'expire' => $cookie_expire,
		);
		set_cookie($cookie);
	}





}
