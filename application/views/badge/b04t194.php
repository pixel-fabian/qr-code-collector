<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_4_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Topinambur</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Die Topinambur-Pflanze ist als essbare kleine Sonnenblume bekannt. Sie blüht im Sommer und wächst ca. 1.50 m hoch. Die Knollen können ab November geerntet werden.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b04_01.jpg" alt="Guetle">
      <figcaption>Topinambur (2017)</figcaption>
    </figure>
    <p>
      Topinambur enthält Kalium, Eisen, Magnesium, Phosphor, Calcium, Zink, Selen, Vitamin B1. Er hat einen hohen Ballaststoff-Anteil sowie Polyphenole, die für ihre antioxidative, entzündungshemmende und krebsvorbeugende Wirkung bekannt sind.
    </p>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
