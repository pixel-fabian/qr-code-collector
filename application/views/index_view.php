<!-- Übersichtseite -->
<div class="row">
  <?php foreach($badges as $badge): ?>
    <div class="col-4-sm">
      <div class="badge-idx <?php if($badge['collected']){ echo('collected'); } ?>">
        <a href="<?php if($badge['collected']){ echo(base_url().'badge/'.$badge['id']); } else { echo('javascript:void(0)'); } ?>">
          <img src="<?= base_url(); ?>assets/media/badges/<?= $badge['img'] ?> " alt="Badge Icon" class="badge__img">
        </a>
      </div>
    </div>
  <?php endforeach; ?>
</div>
