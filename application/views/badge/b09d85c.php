<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_9_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Spargel</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Vor zwei Jahren startete ich den Versuch mit grünem Spargel, er wuchs an und entwickelte sich. In diesem Jahr ernteten wir zum ersten Mal Spargelstangen.
      Es ermutigt mich das Feld zu erweitern.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b09_01.jpg" alt="Guetle">
      <figcaption>Spargel (2018)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
