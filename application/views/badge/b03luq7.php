<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_3_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Zisterne</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Auf unserem Gütle gibt es keinen Wasseranschluss.
      Mit dem Anlegen von dem unteren Gemüsefeld installierte ich im letzten Jahr eine zweite Zisterne mit 1700 Liter.
      Mit vereinten Kräften haben Fabian und ich die zwei Halbschalen vom Anhänger aus über den Zaun gewuchtet.
      Die Befüllung erfolgt über die Dachrinnen der Werkstatt-Hütte darüber. Die Zisterne möchte ich ummanteln und begrünen, u.a. um sie frostsicher zu machen.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b03_01.jpg" alt="Guetle">
      <figcaption>Transport der neuen Zisterne (2017)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b03_02.jpg" alt="Guetle">
      <figcaption>Transport der neuen Zisterne (2017)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
