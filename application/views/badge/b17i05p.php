<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_17_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Württemberg</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Gegenüber siehst du den Württemberg, auch Rotenberg genannt, auf welchem die <a href="https://www.stuttgart-tourist.de/a-grabkapelle-auf-dem-wuerttemberg" target="_blank">Grabkapelle</a> steht. König Wilhelm I. von Württemberg ließ diese zwischen 1820 1824 für seine früh verstorbene Gemahlin Königin Katharina erbauen. Darin sind Königin Katharina Pawlowna (1788-1812), König Wilhelm I. (1781-1864) und ihre Tochter Marie Friederike Charlotte von Württemberg (1816-1887) bestattet. Die Grabkapelle ist den Sommer über zu besichtigen.
    </p>
    <p>
      Weit im Hintergrund links ist das <a href="https://de.wikipedia.org/wiki/Gewa-Tower" target="_blank">Fellbacher Hochhaus "Gewa-Tower"</a> zu sehen. Mit einer Höhe von 107m ist der Gewa-Tower das höchste Hochhaus Baden-Württembergs
    </p>
    <p>
      Von hier aus nach oben, ca. 180m weiter der Straße folgend, gibt es noch einen Aussichtspunkt mit Bänkchen. Die Straße herunter geht es zurück zum Waldheim.
    </p>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <h3>Aussichtspunkt</h3>
    <p>
      Von hier hast du die Mercedes-Benz Arena (Neckarstadion), in der der VfB Stuttgart seine Heimspiele austrägt, gut im Blick. Daneben befindet sich das Mercedes-Benz Museum, sowie das Mercedes-Benz Werk was sich bis nach Untertürkheim dem Neckar entlang zieht.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b17_01.jpg" alt="Guetle">
      <figcaption>Blick auf Neckarstadion und Mercedes-Benz Werk (2018)</figcaption>
    </figure>
    <p>
      An der linken Seite ist noch das Kohlekraftwerk Gaisburg mit dem Schornstein sichtbar. Es wird zurzeit durch ein Gaskraftwerk ersetzt.
    </p>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
