<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Gütle Erkundung</title>

  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/lib/normalize.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.min.css">
</head>
<body>
  <header>
    <div class="header__container clearfix">
      <div class="header__logo">
        <h1>
          <a href="<?= base_url(); ?>index.php">
            <img src="<?= base_url(); ?>assets/media/img/logo.png" alt="Gütle Erkundung">
          </a>
        </h1>
      </div>
    </div>
  </header>
  <main>
    <div class="content__container">

      <?php $this->view($content); ?>

    </div>
  </main>
  <footer>
    <div class="footer__container">
      <nav class="nav-footer">
        <ul class="nav-footer__list">
          <li class="nav-footer__item">
            <a href="http://www.enzoart.de/de/impressum.php" target="_blank" class="nav-footer__link">Impressum</a>
          </li>
        </ul>
      </nav>
    </div>
  </footer>
</body>
</html>
