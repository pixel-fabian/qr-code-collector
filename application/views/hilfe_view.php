<!-- Hilfeseite -->
<h2>Hilfe</h2>

<h3>Vorraussetzungen</h3>
<p>Um beim Erkundungsspiel mitzumachen benötigst du ein Smartphone oder Tablet mit einer QR-Code Scanner-App.</p>
<ul>
  <li>Empfehlung Android: "XYZ" bei f-droid oder im PlayStore</li>
  <li>Empfehlung IOS: "XYZ" im Apple App-Store</li>
</ul>

<h3>So funktionierts</h3>
<ul>
  <li>Suche einen QR-Code rund um das Waldheim Wangen</li>
  <li>Scanne den QR-Code mit der Scanner-App</li>
  <li></li>
</ul>
