<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_13_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Obstbäume</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Die ersten Obstbäume pflanzten Fabian und ich 1999, im Laufe der Jahre kamen weitere Bäume dazu.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b13_01.jpg" alt="Guetle">
      <figcaption>Heinz & Fabian (1999)</figcaption>
    </figure>
    <p>
      Was dem „Überleben“ und besonders den jungen Bäumen bedrohlich zusetzt, ist der Wildverbiss.
      Die zarten Blätter und Rinde auch an den Sträuchern wird von Rehen angeknabbert. Ihre V-Abdrücke sind im feuchten Boden deutlich zu erkennen. Im Frühjahr habe ich drei Rehe im Gütle gesehen.
    </p>
    <p>
      In diesem Jahr machte sich die Gespinstmotte über Apfelbäume her, die Blätter einiger End- und Seitentriebe waren mit einem dichten weißen Gespinst umsponnen. So habe sie abgeschnitten, es gibt dieses Jahr keinen Ertrag.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b13_02.jpg" alt="Guetle">
      <figcaption>Fabian & Fips (1996)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
