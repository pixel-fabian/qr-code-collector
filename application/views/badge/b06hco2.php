<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_6_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Werkstatt</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Als wir das Gütle rodeten, tauchte hier eine Grundmauer auf, die wahrscheinlich für die Erstellung einer Hütte gedacht war.
      Im ersten Jahr stellten wir hier ein Zelt auf in dem wir unsere Gartengeräte verwahrten.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b06_01.jpg" alt="Guetle">
      <figcaption>Gerätezelt (1997)</figcaption>
    </figure>
    <p>
      1998 baute ich eine erste Überdachung und benutze es als Atelier.
      Im Laufe der Jahre habe ich es immer wieder erweitert und verändert, jetzt in der Pension dient es mir als Werkstatt und Atelier.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b06_02.jpg" alt="Guetle">
      <figcaption>Atelier (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b06_03.jpg" alt="Guetle">
      <figcaption>Atelier (2002)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
