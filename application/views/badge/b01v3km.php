<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_1_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Historie</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Das Grundstück mit 7,5 Ar haben wir im Oktober 1996 erworben. Es war total zugewuchert mit Brombeerhecken und sonstigem meterhohen Gestrüpp.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b01_01.jpg" alt="Guetle">
      <figcaption>Fabian & Franz (1996)</figcaption>
    </figure>
    <p>
      Mein Vater und meine Brüder legten kräftig mit Hand an, um das Stück begehbar zu machen. Die Nachbarn waren beeindruckt vom „Maschinenpark“ von Franz, „sind die vom Gartenbauamt?“ erkundigten sie sich. Sichtbar wurden die typischen Weinbergterrassen mit den Natursteinmauern.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b01_02.jpg" alt="Guetle">
      <figcaption>Gestrüpp (1996)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b01_03.jpg" alt="Guetle">
      <figcaption>Ursula, Heinz, Martin & Franz (1996)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
