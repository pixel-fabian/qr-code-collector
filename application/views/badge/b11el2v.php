<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_11_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Gemüsefeld</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      1998 haben wir hier das erste Feld angelegt und Mais und Kartoffeln gepflanzt. Im Laufe der Zeit kamen Kürbisse, Tomaten und Kräuter dazu.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b11_01.jpg" alt="Guetle">
      <figcaption>Erster Maisanbau (1998)</figcaption>
    </figure>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b11_02.jpg" alt="Guetle">
      <figcaption>Anlegen des Gemüsefeldes (2006)</figcaption>
    </figure>
    <p>
      Im letzten Jahr habe ich eine Überdachung für die Tomaten gebaut. Die Artischocken davor wachsen im zweiten Jahr.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b11_03.jpg" alt="Guetle">
      <figcaption>Artischocke (2018)</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
