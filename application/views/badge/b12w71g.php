<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_12_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Windharfe</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      In der 8. Klasse führt in der Waldorfschule jeder Schüler eigenständig ein größeres Projekt durch, eine sogenannte Jahresarbeit.
      Fabian entschied sich mit Hilfe von Heinz eine Windharfe (auch Äolsharfe genannt) zu bauen. Das Instrument steht geschützt in dieser kleinen Hütte.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b12_01.jpg" alt="Guetle">
      <figcaption>Windarfe (2006)</figcaption>
    </figure>
    <p>
      Wenn der Wind die Saiten mit bestimmten Frequenzen in Schwingung versetzt, können die soganannten Obertöne hörbar werden. Man kennt dieses Phänomen der Windharfe auch von alten (hölzernen) Telegrafenmasten.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b12_02.jpg" alt="Guetle">
      <figcaption>Grundschwigung (a) und Oberschwingungen (b, c) einer Saite</figcaption>
    </figure>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
