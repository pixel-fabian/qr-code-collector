<!-- Detailseite Badge 01-->
<div class="row badge__top">
  <div class="col-4-sm">
    <div class="badge__icon">
      <img src="<?= base_url(); ?>assets/media/badges/badge_14_aktiv.jpg" alt="" class="badge__img">
    </div>
  </div>
  <div class="col-8-sm">
    <h2 class="badge__title">Fritz & Freya</h2>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <p>
      Zweimal im Jahr treffe ich mich mit einer Gruppe über ein langes Wochenende bei Clavigo im Schwarzwald. Wir sind die <a href="http://grabsteingruppe.de/" target="_blank">Grabsteingruppe</a> und setzen uns mit der Frage „Leben und Sterben“ auseinander. Das Arbeiten am Stein verstehen wir wie eine Metapher für die Lebenswirklichkeit des bildhauernden Menschen auf der Suche nach sich selbst.
    </p>
    <figure>
      <img src="<?= base_url(); ?>assets/media/img/b14_01.jpg" alt="Guetle">
      <figcaption>Fritz und Freya (2018)</figcaption>
    </figure>
    <p>
      Aus einem entsorgten Grabstein formte ich über einen längeren Prozess die Figur Fritz, ein Fabelwesen mit Käppi, Flügeln und Raubtierkrallen. Alleine wirkte er verloren, er hatte einen unsicheren Stand. Deshalb bekam er als Begleitern und Stütze Freya. Diese Grabsteinformation habe ich zur Ausstellung in Büsingen „LEBEN UND STERBEN L.u.St.“ im Juli 2015 abgeschlossen. Die Beiden stehen nun hier und schauen in das Tal und begleiten uns auf dem Gütle.
    </p>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <a href="<?= base_url(); ?>" class="button">zur Übersicht</a>
  </div>
</div>
