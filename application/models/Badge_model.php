<?php
class Badge_model extends CI_Model {

  /**
   * Konstruktur
   */
  public function __construct() {

  }

  var $badge_list = array(
    'b00ix26',
    'b01v3km',
    'b02gr8h',
    'b03luq7',
    'b04t194',
    'b05wb51',
    'b06hco2',
    'b07j33e',
    'b08f4z3',
    'b09d85c',
    'b10sxv9',
    'b11el2v',
    'b12w71g',
    'b13xop4',
    'b14y4u0',
    'b15r67f',
    'b16vz29',
    'b17i05p',
  );


  /**
   * Alle Badges laden
   * @return array Badges
   */
  public function get_all_badges() {
    return $this->badge_list;
  }

  /**
   * Prüfen ob Badge existiert
   */
  public function badge_exists($id) {
    if (in_array($id, $this->badge_list)) {
      return TRUE;
    } else {
      return FALSE;
    }
  }



}
